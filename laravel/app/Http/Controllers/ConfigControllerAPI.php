<?php

namespace App\Http\Controllers;

use App\Config;
use App\Http\Requests\UpdateEmailFormRequest;

class ConfigControllerAPI extends Controller
{

    public function show($id)
    {
        return Config::findOrFail($id);
    }

    public function update(UpdateEmailFormRequest $request, $id)
    {
        $config = Config::findOrFail($id);
        $config->platform_email = $request->email;
        $config->save();
        return response()->json('Platform email updated with success!', 200);

    }

}
